﻿CREATE TABLE [dbo].[Users] (
    [Id]       INT           NOT NULL,
    [Name]     NVARCHAR (10) NOT NULL,
    [Email]    NVARCHAR (50) NOT NULL,
    [Password] NVARCHAR (10) NOT NULL,
    [Birthday] DATETIME2 (7) NOT NULL,
    [Gender]   BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

