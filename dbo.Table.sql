﻿CREATE TABLE [dbo].[Table] (
    [Id]       INT           NOT NULL,
    [Name]     NVARCHAR (10) NOT NULL,
    [Email]    NVARCHAR (50) NOT NULL,
    [Password] NVARCHAR (10) NOT NULL,
    [Birthday] DATETIME2 (7) NOT NULL,
    [Gender]   BIT           NOT NULL IDENTITY,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

